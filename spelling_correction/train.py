from argparse import ArgumentParser
import os

from utils.pre_processing import *
from utils.utils import *
from model.datamodule import CharDataset
from model.encode_decode_transformer import Transformer, TransformerConfig
from utils.trainer import Trainer, TrainerConfig

import torch

parser = ArgumentParser('spelling_correction')

# learning hyperparameters
parser.add_argument('--sequence_len', type=int, default=128, help='maximal sequence length')
parser.add_argument('--min_len', type=int, default=5, help='minimal sequence length')
parser.add_argument('--lr', type=float, default=6e-4, help='learning rate')
parser.add_argument('--grad_clip', type=float, default=1.0, help='gradient clipping value')
parser.add_argument('--bs', type=float, default=32, help='batch size')
parser.add_argument('--num_epochs', type=int, default=1, help='number of epochs')

# model hyperparameters
parser.add_argument('--embed_dim', type=int, default=128, help='embedding dimensional')
parser.add_argument('--n_block', type=int, default=4, help='number of transformer blocks')
parser.add_argument('--n_head', type=int, default=8, help='number of attention heads')

parser.add_argument('--data_path', type=str, required=True, help='data path')
parser.add_argument('--preprocessed_data_path', type=str, default='./data/cleaned/', help='preprocessed data path')
parser.add_argument('--device', type=str, default='cuda' if torch.cuda.is_available() else 'cpu', help='gpu flag')


args = parser.parse_args()

def main():
    global args
    print(args)
    min_len = args.min_len
    sequence_len = args.sequence_len

    # process and save data
    print('start loading data')
    path = os.path.join('data', args.data_path)
    ckpt_path = os.path.join('checkpoint', args.data_path)
    if not os.path.exists(ckpt_path):
        os.mkdir(ckpt_path)
    
    source, target = list(), list()
    x = open(os.path.join(path, 'source.txt'), encoding='utf-8').read().split("\n")
    y = open(os.path.join(path, 'target.txt'), encoding='utf-8').read().split("\n")
    x,y = pre_processing(x, y, min_length=min_len, max_length=sequence_len) # remove sentence less than 5 characters
    source += x
    target += y

    path = args.preprocessed_data_path
    pickle(path+"source", source)
    pickle(path+"target", target)
    print('data preprocessed')
    print('-' * 50)

    source, target = pre_processing(source, target, min_length=min_len, max_length=sequence_len) # clip sentences
    print('data loaded')
    print('-' * 50)

    dataset = CharDataset(target, source, sequence_len=sequence_len)
    torch.save(dataset, os.path.join(ckpt_path, 'dataset'))

    # building model
    tconfig = TrainerConfig(max_epochs=args.num_epochs, batch_size=args.bs, learning_rate=args.lr, grad_norm_clip=args.grad_clip, device=args.device,
                        lr_decay=True, warmup_tokens=5000, ckpt_n_print_iter=2000, ckpt_path=os.path.join(ckpt_path, 'ckpt'))

    mconfig = TransformerConfig(vocab_size=dataset.vocab_size, sequence_len=dataset.sequence_len, embed_dim=args.embed_dim,
                            n_block=args.n_block, n_head=args.n_head, device=tconfig.device)
    torch.save(tconfig, os.path.join(ckpt_path, 'tconfig'))
    torch.save(mconfig, os.path.join(ckpt_path, 'mconfig'))
    
    model = Transformer(mconfig)

    # building trainer
    sentences = ['Cắtt tóc và tran đỉm', 'bacs sĩi', 'nawng dông', 'cong nhan', 'bảoo về', 'nhân vên mảkerting', 'biet chúp ảnhh', 'tái xế', 'nhân vin ann linh', 'lái xê', 'nhầ tầu']
    trainer = Trainer(model, dataset, tconfig, test_dataset=sentences, collate=None)


    # load pre-trained weights
    # model.load_state_dict(pickle(tconfig.ckpt_path)) # load

    # train
    print('start training')
    trainer.train()
    print('done')


if __name__ == '__main__':
    main()