import torch
from torch.utils.data import Dataset
class CharDataset(Dataset):
    '''
    Dataset is a iterable that returns input and target sentence. It adds <sos> at the begining, and <eos> at the end, 
    and filling in <pad> if sentence length is less than pre-defined value.
    '''
    
    def __init__(self, x, y, sequence_len, encoder=None):
        # data in the type of pairs of sentence
        data = ''.join(x+y)
        # from collections import Counter
        # vocab_size = 250

        # ct = Counter(data)
        # include = sorted(ct, key=ct.get, reverse=True)
        # if len(include)>vocab_size: include = include[:vocab_size]
        # rule = ''.join(include)
        chars = ['<pad>'] +['<sos>'] + ['<eos>'] + sorted(list(set(data)))
        data_size, vocab_size = len(data), len(chars)

        print('data has %d characters, %d unique chars, %d sentences.' % (data_size, len(chars), len(x)))
        print('sentence length nine_nine_percentile: %d' % (sequence_len))
        
        self.x, self.y = x, y
        self.ch2i = {ch:i for i,ch in enumerate(chars)}
        self.i2ch = {i:ch for i,ch in enumerate(chars)}
        self.vocab_size = vocab_size
        self.sequence_len = sequence_len
        self.encoder=encoder
    
    def __len__(self):
        return len(self.x) # len x = y
    
    def __getitem__(self, idx):
        
        indx = self.padding([self.ch2i[ch] for ch in self.x[idx]] + [self.ch2i['<eos>']])
        indy = [self.ch2i['<sos>']] + self.padding([self.ch2i[ch] for ch in self.y[idx]] + [self.ch2i['<eos>']])

        x = torch.tensor(indx, dtype=torch.long)
        y = torch.tensor(indy, dtype=torch.long)

        return x,y
                                                                                                                               
    def padding(self, string):
        if len(string)<self.sequence_len:
            string =  string + [0]*(self.sequence_len - len(string))
        else:
            string = string[:self.sequence_len -1] + [self.ch2i['<eos>']]
                   
        return string