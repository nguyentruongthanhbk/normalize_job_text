import os

import torch
import argparse

from model.datamodule import CharDataset

parser = argparse.ArgumentParser()
parser.add_argument('--source_path', type=str, required=True, help='source path')
parser.add_argument('--ckpt', type=str, required=True, help='checkpoint path')
parser.add_argument('--device', type=str, default='cuda' if torch.cuda.is_available() else 'cpu', help='device name')

args = parser.parse_args()

def clean(text, letters):
    new_text = ''
    for s in text:
        if new_text == '' and s == '.':
            continue
        if s in letters:
            new_text += s
    new_text = new_text.strip()
    return new_text

def main():
    global args
    path = os.path.join('checkpoint', args.ckpt)
    dataset_path = os.path.join(path, 'dataset')
    print('checkpoint loaded')

    dataset = torch.load(dataset_path)
    letters = list(dataset.ch2i.keys())


    path = './inference/' + args.source_path
    target_path = './inference/' + 'cleaned_' + args.source_path
    with open(path) as f:
        lines = [line.strip() for line in f.readlines()]

    clean_lines = [clean(line, letters) for line in lines]

    with open(target_path, 'w') as f:
        for clean_line in clean_lines:
            f.write(clean_line + '\n')


if __name__ == '__main__':
    main()
