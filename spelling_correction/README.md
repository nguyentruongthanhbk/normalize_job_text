Mô hình sử dụng cho bài toán là character-level encoder-decoder.
Nhắc lại: từ quá trính làm dữ liệu từ folder noisy_text thu được source.txt (nhãn) và target.txt (inputs)

1.Xây dựng thư mục chứa dữ liệu theo đúng cấu trúc sau
data
--> dataset_name
----> source.txt
----> data.txt

2. Huấn luyện model sử dụng câu lệnh:
>>> python train.py --data_path dataset_name

Trong quá trình huấn luyện, một số câu thuộc list sentences sẽ được inference để theo dõi chất lượng mô hình huấn luyện
Ngoài ra sau khi inference, trainer sẽ lưu lại checkpoint tại ./checkpoint/dataset_name, checkpoint bao gồm 4 thành phần:
+ dataset (có chứa encoder, character to index, phục vụ inference sau này)
+ tconfig: trainer config
+ mconfig: model config
+ ckpt: tham số mô hình

Ngoài --data_path ta có các hyperparameters sau để tuning:
optional arguments:
  --sequence_len SEQUENCE_LEN
                        maximal sequence length
  --min_len MIN_LEN     minimal sequence length
  --lr LR               learning rate
  --grad_clip GRAD_CLIP
                        gradient clipping value
  --bs BS               batch size
  --num_epochs NUM_EPOCHS
                        number of epochs
  --embed_dim EMBED_DIM
                        embedding dimensional
  --n_block N_BLOCK     number of transformer blocks
  --n_head N_HEAD       number of attention heads
  --data_path DATA_PATH
                        data path
  --preprocessed_data_path PREPROCESSED_DATA_PATH
                        preprocessed data path
  --device DEVICE       gpu flag
  
3. Inference
>>> python pre_infer.py --source_path filename --ckpt dataset_name --> tạo ra 1 file cleaned_filename tại folder inference, dùng cho infer sau này
>>> python inference.py --source_path filename --ckpt dataset_name

Lưu ý, file cần infer filename sẽ lưu tại folder inference theo cấu trúc sau:
inference
--> filename

File infer sẽ được lưu ngay tại folder inference với tên infered_filename:
inference
--> filename
--> cleaned_filename
--> infered_filename
