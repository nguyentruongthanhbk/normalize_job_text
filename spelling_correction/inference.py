import os

import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
import argparse

from utils.trainer import Trainer, TrainerConfig
from utils.utils import pickle
from utils.utils import *
from utils.pre_processing import *
from model.encode_decode_transformer import Transformer, TransformerConfig
from model.datamodule import CharDataset

parser = argparse.ArgumentParser()
parser.add_argument('--source_path', type=str, required=True, help='source path')
parser.add_argument('--ckpt', type=str, required=True, help='checkpoint path')
parser.add_argument('--device', type=str, default='cuda' if torch.cuda.is_available() else 'cpu', help='device name')

args = parser.parse_args()

def main():
    global args
    path = os.path.join('checkpoint', args.ckpt)
    dataset_path = os.path.join(path, 'dataset')
    tconfig_path = os.path.join(path, 'tconfig')
    mconfig_path = os.path.join(path, 'mconfig')
    ckpt_path = os.path.join(path, 'ckpt')
    print('checkpoint loaded')

    dataset = torch.load(dataset_path)
    tconfig = torch.load(tconfig_path)
    mconfig = torch.load(mconfig_path)
    mconfig.device = args.device

    # building model
    model = Transformer(mconfig)

    # load pre-trained weights
    model.load_state_dict(torch.load(ckpt_path, map_location = torch.device(args.device))) # load

    path = './inference/cleaned_' + args.source_path
    target_path = './inference/' + 'infered_' + args.source_path
    model.to(args.device)

    with open(path) as f:
        lines = [line.strip() for line in f.readlines()]

    lines = lines[:200]
    results = model.generate_output(lines, dataset, top_k=5, temperature=1.0, print_process=True)

    with open(target_path, 'w') as f:
        for line, result in zip(lines, results):
            f.write(line + ' --> ' + result + '\n')


if __name__ == '__main__':
    main()
