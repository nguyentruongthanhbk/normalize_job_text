letters=list("abcdefghijklmnopqrstuvwxyzáàảãạâấầẩẫậăắằẳẵặóòỏõọôốồổỗộơớờởỡợéèẻẽẹêếềểễệúùủũụưứừửữựíìỉĩịýỳỷỹỵđABCDEFGHIJKLMNOPQRSTUVWXYZÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÉÈẺẼẸÊẾỀỂỄỆÚÙỦŨỤƯỨỪỬỮỰÍÌỈĨỊÝỲỶỸỴĐ ,;.")

def clean(text):
    text = text.replace('/', ' ').replace('\\', ' ').replace('-', ' ').replace(';', ',').replace('.', ',')
    new_text = ''
    for s in text:
        if s in letters:
            new_text += s
    new_text = new_text.strip()
    new_subtext = [subtext.strip() for subtext in new_text.split(',')]
    return new_subtext
with open('./raw.txt') as f:
    lines = f.readlines()

cleaned_lines = []

for line in lines:
    cleaned_line = clean(line)
    if len(cleaned_line) != 0:
        for subtext in cleaned_line:
            if subtext != '':
                cleaned_lines.append(subtext)

with open('./data.txt', 'w') as f:
    for cleaned_line in cleaned_lines:
        f.write(cleaned_line + '\n')
