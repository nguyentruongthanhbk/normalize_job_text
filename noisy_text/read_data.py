letters=list("abcdefghijklmnopqrstuvwxyzáàảãạâấầẩẫậăắằẳẵặóòỏõọôốồổỗộơớờởỡợéèẻẽẹêếềểễệúùủũụưứừửữựíìỉĩịýỳỷỹỵđABCDEFGHIJKLMNOPQRSTUVWXYZÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÉÈẺẼẸÊẾỀỂỄỆÚÙỦŨỤƯỨỪỬỮỰÍÌỈĨỊÝỲỶỸỴĐ ,;.1234567890")

def clean(text):
    new_text = ''
    for s in text:
        if new_text == '' and s == '.':
            continue
        if s in letters:
            new_text += s
    new_text = new_text.strip()
    return new_text
with open('./raw.txt') as f:
    lines = f.readlines()

cleaned_lines = []

for line in lines:
    clean_line = clean(line)
    if clean_line != '':
        cleaned_lines.append(clean(line))

with open('./data.txt', 'w') as f:
    for cleaned_line in cleaned_lines:
        f.write(cleaned_line + '\n')
