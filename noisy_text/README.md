Quá trình làm dữ liệu:
1. Dữ liệu được lấy từ 2 nguồn:
- file định nghĩa kĩ năng của từng ngành nghề (jobs.xlxs) --> raw.txt (dữ liệu này có chính tả sạch sẽ nhưng nhiều từ ngữ hơi xa vời)
- file đánh nhãn ngành nghề và kĩ năng (ent_tagged.yml) --> jobs_data.txt và skills_data.txt (dữ liệu này rất tốt vì từ domain thật, nhưng sẽ phải tốn công làm sạch chính tả thủ công)

2. Từ các file raw.txt và jobs_data.txt làm sạch chính tả bằng read_data.py, thu được tương ứng data.txt (4195 đoạn text) và data_2.txt (3884 đoạn text) (skills_data.txt khá dài nên tạm thời chưa làm sạch chính tả, có thể lợi dụng một phần nhỏ làm test data?)

! Lưu ý, khi read_data.py cho raw.txt loại bỏ các kí tự số trong letters vì file raw.txt chứa kí tự số chỉ mang ý nghĩa đề mục chứ không mang nghĩa số lượng.

3. Từ data.txt và data_2.txt ta đi xây dựng các bộ dữ liệu như sau:
- chạy lệnh ">>> python add_noise.py" thu được 2 bộ source.txt và target.txt, trong đó source.txt phục vụ như nhãn vì nó đúng chính tả, target.txt phục vụ như input cho mô hình vì nó sai chính tả

! add_noise.py được xây dựng trên rất nhiều tham số, trong đó có những tham số sau quan trọng:
	"
	cleaned_texts = cleaned_texts * 500
	cleaned_texts_2 = cleaned_texts_2 * 1000
	"
- 500 và 1000 mang nghĩa là số lần duplicate của data.txt và data_2.txt. Như vậy có tổng tất cả 500 * 4195 + 1000 * 3884 = 5.981.500 dữ liệu, trong đó lấy 1000 duplicate bộ data_2.txt vì đây là dữ liệu từ domain thật (thực tế cho thấy mô hình sau khi train với cả data này sửa lỗi tốt hơn, đặc biệt là lỗi ngữ nghĩa)
- Dữ liệu mặc dù duplicate nhưng mỗi lần có khả năng sai chính tả khác nhau (hoặc đôi khi giữ nguyên), được kiểm soát bởi các threshold, trong đó có 2 threshold quan trọng nhất là 0.90 và 0.975 ở đoạn code sau:
	"
	with open(target_path, 'w', encoding='utf-8') as f:
	    for text in tqdm(all_cleaned_texts):
		f.write(add_noise(text, 0.90, 0.975))
	"
	+ với 0.90 là 90 % kí tự không bị thay đổi
	+ 0.975 - 0.9 = 0.075 mang nghĩa 7.5% kí tự bị lỗi chính tả
	+ 2.5% còn lại mang nghĩa, kí tự có khả năng bị tráo vị trí, hoặc bị gõ nhầm thành một kí tự ngẫu nhiên
	
--> tuning 500 và 1000 để cho phép thành phần dữ liệu từ domain thật hoặc được định nghĩa, phần nào nhiều hơn
--> tuning 0.9 và 0.975 để cho phép text bị sai chính tả nhiều hay ít (từng loại lỗi sai có threshold riêng, xem chi tiết trong code)

! Lưu ý 1: add_noise.py đang bị giới hạn bởi 2 file data.txt và data_2.txt, 2 file texts rất đặc trưng cho domain nghề nghiệp. Hoàn toàn có thể xây dựng bộ dữ liệu ở domain khác, hoặc thậm chí một bộ dữ liệu sai chính tả chung, không bị giới hạn domain (crawl các đoạn text từ wiki). Tất nhiên để làm một mô hình sửa chính tả tổng quát cần một bộ dữ liệu rất lớn và tài nguyên rất lớn để huấn luyện mô hình.

! Lưu ý 2: Spelling correction là một bài toán tương đối khó, nên chất lượng các mô hình hiện giờ chưa được tốt, có thể survey các github đã xử lý bài toán này, chất lượng chỉ tương đối. Hướng giải quyết:
Thường xuyên huấn luyện mô hình với bộ dữ liệu được xây dựng như sau:
+ Dữ liệu cũ, chiếm phần trăm lớn, để mô hình tránh quên những gì đã học được
+ Dữ liệu mới, chiếm phần trăm nhỏ, mô hình chưa từng gặp, để mô hình học được từ mới

