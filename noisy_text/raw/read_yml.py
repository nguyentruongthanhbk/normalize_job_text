import yaml
from yaml.loader import SafeLoader

path = './ent_tagged.yml'

def extract_ett(txt):
    jobs_list = []
    skills_list = []
    
    l = len(txt)
    start = 0
    end = 0
    
    while start < l:
        if txt[start] == '[':
            while end < l:
                if txt[end] == ')':
                    open_pos = txt[start:end + 1].find('(')
                    if 'nghe nghiep' in txt[start:end + 1]:
                        jobs_list.append(txt[start + 1:start + open_pos - 1])
                    elif 'ky nang' in txt[start:end + 1]:
                        skills_list.append(txt[start + 1:start + open_pos - 1])
                    break
                end += 1
            start = end
        start += 1
        end += 1
    
    return jobs_list, skills_list
            

with open(path) as f:
    raw = yaml.load(f, Loader=SafeLoader)

data = raw['nlu'][0]['examples']

jobs_list = []
skills_list = []

for item in data:
    tmp_jobs_list, tmp_skills_list = extract_ett(item['text'])
    jobs_list.extend(tmp_jobs_list)
    skills_list.extend(tmp_skills_list)

jobs_path = './jobs_data.txt'
skills_path = './skills_data.txt'

with open(jobs_path, 'w') as f:
    for job in jobs_list:
        f.write(job + '\n')

with open(skills_path, 'w') as f:
    for skill in skills_list:
        f.write(skill + '\n')